/**
 * IMPORT core modules
 */
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');

/**
 * IMPORT router modules
 */
const dishRouter = require('./routes/dishRouter');

/**
 * VARIABLES
 */
const hostname = 'localhost';
const port = 3000;

// This is an express server
const app = express();

// The morgan module is used to print the logs in the console
app.use(morgan('dev'));

// The body is analyzed using the body-parser module
// The body of the incoming request will be parsed and then added into the req object as req.body
app.use(bodyParser.json());




/**
 * ROUTER of /dishes/id  
 */
app.get('/dishes/:dishId', (req,res,next) => {
    res.end('Will send details of the dish: ' + req.params.dishId + ' to you');
});

app.post('/dishes/:dishId', (req,res,next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /dishes/' + req.params.dishId);
});

app.put('/dishes/:dishId', (req,res,next) => {
    res.write('Updating the dish: ' + req.params.dishId + '\n');
    res.end('Will update the dish: ' + req.body.name + ' with details: ' + req.body.description);
});

app.delete('/dishes/:dishId', (req,res,next) => {
    res.end('Deleting dish: ' + req.params.dishId);
});


/**
 * STATIC FILES
 */
app.use(express.static(__dirname + '/public'))

// Any request coming to that /dishes endpoint will be handled by dishRouter 
app.use('/dishes', dishRouter);

// Manage requested pages which are not in the static files directory
app.use((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<html><body><h1> This is an Express Server </h1></body></html>');
})

/**
 * CREATE SERVER 
 */
const server = http.createServer(app);
server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}`);
})