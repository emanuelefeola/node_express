const express = require('express');
const bodyParser = require('body-parser');

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());

/**
 * ROUTER of /dishes 
 */
dishRouter.route('/')
    .all((req,res,next) => { // .all -> no matter which method is invoked
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next(); // check down below if there are more istructions for the '/dishes' path
    })

    .get((req,res,next) => {
        res.end('Will send all the dishes to you!');
    })

    .post((req,res,next) => {
        res.end('Will add the dish: ' + req.body.name + ' with details: ' + req.body.description);
    })

    .put((req,res,next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /dishes');
    })

    .delete((req,res,next) => {
        res.end('Deleting all the dishes!');
    });

module.exports = dishRouter;